console.log(`Hello world`);


// mini act
console.warn(`Drink html`);
console.warn(`Eat javascript`);
console.warn(`Inhale css`);
console.warn(`Bake bootstrap`);

function arrs(){
	let arr1 = [`Drink html`, `Eat javascript`, `Inhale css`, `Bake bootstrap`];
	console.warn(arr1);

	let g1 = [1,2,3,4,5,6];
	let arr2 = [`Marvin`, `Orias`, `1234560`];
	console.warn(arr2);
	arr2[2] = `aadasdasd`;
	console.warn(arr2);
	console.warn(arr2.length);

	if (arr2.length > 1){
		console.log(`Hey`);
	}

	let arr3 = arr2.length - 1;
	console.log(arr2[arr3]);

	// array methods
	// push -- adds an element in the end index

	let arr4 = [1,2,3,4,5,6];
	console.log(arr4);
	arr4.push(7,8,9);
	console.log(arr4);

	//pop -- removes tbe last elem
	let arr5 = arr4.pop();
	console.log(arr5);
	console.log(arr4);

	// unshift -- add in the beginning index
	console.log(arr4);
	arr4.unshift(14, 0, 123);
	console.log(arr4);

	// shift -- removes first element index
	arr4.shift()
	arr4.shift();
	console.log(arr4);

	// splice -- removes specified index and adds an index
	console.log(arr4);
	arr4.splice(1,0, 155, 124, 2415);
	console.log("Splice" + arr4);
	arr4.splice(0, 3);
	console.log("Splice" + arr4);

	// sort -- ordering of elems
	arr5 = [`s`, `a`, `d`,`q`,`b`, `a`];
	console.log(arr5.sort());

	// reverse -- reversing ordeer of elems
	 console.log(arr5.reverse());

	//indexof -- returns index number of matched elem
	// console.log(arr5.indexOf(`a`, 2));
	console.log(arr5.indexOf(`a`));
	console.log(arr2.indexOf(`ori`));

	// lastIndexOf() -- return index of last matching elem
	console.log(arr5.lastIndexOf(`a`));

	// slice -- removes element(s) in array
	console.log("Slice" + arr5.slice(3));
	console.log("Slice" + arr5.slice(3, 5));

	// tostring -- returns to array, separated by commas
	console.log(arr5.toString());

	// concat -- combines elems in array
	console.log(arr5.concat(arr2, arr4));

	// join returns an array as a string separated by specified separator string
	console.log(arr5.join());
	console.log(arr5.join(` `));
	console.log(arr5.join(`, `));

	// foreach -- iterates on each array elem
	arr5.forEach(function(task) {
		console.log(task);
	});

	// foreach w condition
	let foreachs = [];

	arr5.forEach(function(task){
		if (task.length > 0){
			foreachs.push(task);
		}
	})
	console.log(foreachs);

	// map -- iterates and returns a new array with a diff value depending on the result of the function operation
	let n1 = [1,2,3,4,5,6,7,8];
	let map1 = n1.map(function(number){
		return number * number;
	});
	console.log(map1);

	// every -- checks if all elems match the condition
	let allValid = n1.every(function(number){
		return (number < 3);
	});
	console.log(allValid);

	// some -- checks if at least one elem matches the cond
	let somes = n1.some(function(nums){
		return (nums < 4);
	});
	console.log(somes);

	// filter -- returns the array that matches the cond
	let filts = n1.filter(function(nums){
		return (nums < 4)
	});
	console.log(filts);
	let e1 = [];
	filts = n1.forEach(function(num){
		if(num < 3){
			e1.push(num);
		}
	});
	console.log(e1);

	// include -- returns true if it has a matching item in array, otherwise false
	let incls = arr2.filter(function(nums){
		return nums.includes(`a`);
	});
	console.log(incls);

	// multi arrays
	let m1 = [
		[1,2,3,4,5,6,7,8],
		[34,7,9,0,6,4,6,34],
		[6,7,8,9,0,2,3,3]
	];

	console.log(m1);
}

arrs();